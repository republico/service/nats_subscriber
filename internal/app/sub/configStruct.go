package sub

type ConfigStruct struct {
	EnvironmentVariableAccount string
	EnvironmentVariableKey string
	ServerNats string
	Queue string
	Subject string
	SeparatorSubject string
	Timeout int
	WaitTimeout bool
	SizeLimit int
	Interval int
	MaxTime int
}

var Config ConfigStruct