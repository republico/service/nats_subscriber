package sub

import (
	"os"
	"strings"
	"time"

	"github.com/nats-io/go-nats"

	updateDataLake "gitlab.com/republico/library/go/azure/pkg/update_data_lake"
	"gitlab.com/republico/library/go/util/pkg/exception"
)

func Subscribe() error {
	flagWaitTimeout := !Config.WaitTimeout
	startTime := time.Now()
	timeout := startTime.Add(time.Second * time.Duration(Config.Timeout))
	maxTime := startTime.Add(time.Second * time.Duration(Config.MaxTime))

	nc, err := nats.Connect(Config.ServerNats)
	if err != nil {
		return err
	}
	defer nc.Close()

	updateDataLake.Config.Account = os.Getenv(Config.EnvironmentVariableAccount)
	updateDataLake.Config.Key = os.Getenv(Config.EnvironmentVariableKey)

	var container string
	var blob string
	var record string

	countMessages := 0

	_, err = nc.QueueSubscribe(Config.Subject, Config.Queue, func(message *nats.Msg) {
		if Config.WaitTimeout {
			flagWaitTimeout = true
			timeout = time.Now().Add(time.Second * time.Duration(Config.Timeout))
		}

		subjectArray := strings.Split(message.Subject, Config.SeparatorSubject)

		container = subjectArray[0]
		blob = subjectArray[1]
		dataType := subjectArray[2]

		data := ""
		if dataType == "csv" {
			header := subjectArray[3]
			data = readCsv(header, string(message.Data))
		}

		timeout = timeout.Add(time.Second * time.Duration(Config.Timeout))
		countMessages += 1

		byteSize := len(record + "\n" + data) * 8
		if byteSize > Config.SizeLimit {
			updateDataLake.Execute(container, blob, record)

			record = data + "\n"
		} else {
			record += data + "\n"
		}

		if countMessages == Config.Interval {
			updateDataLake.Execute(container, blob, record)

			record = ""
			countMessages = 0
		}
	})
	exception.HandlerError(err)
	nc.Flush()

	err = nc.LastError()
	if err != nil {
		return err
	}

	ticker := time.NewTicker(500 * time.Millisecond)
	for timeNow := range ticker.C {
		if Config.Timeout > 0 && flagWaitTimeout {
			if timeNow.After(timeout) {
				break
			}
		}

		if Config.MaxTime > 0 {
			if timeNow.After(maxTime) {
				break
			}
		}
	}

	if record != "" {
		updateDataLake.Execute(container, blob, record)
	}

	nc.Close()

	return nil
}