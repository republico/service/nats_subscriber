package sub

import (
	"strings"
)

func readCsv(header string, data string) string {
	headerArray := strings.Split(header, ";")
	dataArray := strings.Split(data, ";")

	json := "{"
	for key, header := range headerArray {
		data := dataArray[key]

		json += header + ":" + data + ","
	}
	json = json[:len(json) - 1]
	json += "}"

	return json
}