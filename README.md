# NATS (Service)

## Depuração

```
go run cmd/sub/main.go -account="ACCOUNT_AZURE" -key="KEY_AZURE" -server="localhost:4222" -queue="republico" -subject="*" -separatorSubject="|" -timeout="5" -wait="true" -size="1024" -interval="10" -maxTime="0"
```

## Testes

### Testes unitários

```
go test ./test -v -run TestSubscribe
```

### Testes do executável

```
sub -account="AZURE_ACCOUNT" -key="AZURE_KEY" -server="localhost:4222" -queue="republico" -subject="*" -separatorSubject="|" -timeout="5" -wait="true" -size="1024" -interval="10" -maxTime="0"
```

## Build

```
GOOS=windows GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/sub.exe cmd/sub/*.go
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static" -s -w' -o bin/sub cmd/sub/*.go
```