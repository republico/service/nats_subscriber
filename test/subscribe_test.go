package test

import (
	"testing"

	"gitlab.com/republico/service/nats_subscriber/internal/app/sub"
)

func TestSubscribe(t *testing.T) {
	sub.Config = sub.ConfigStruct{
		EnvironmentVariableAccount: "ACCOUNT_AZURE",
		EnvironmentVariableKey: "KEY_AZURE",
		ServerNats: "localhost:4222,localhost:4223,localhost:4224",
		Queue: "republico",
		Subject: "*",
		Timeout: 5,
		WaitTimeout: true,
		SizeLimit: 1024,
		Interval: 10,
		MaxTime: 0,
	}

	err := sub.Subscribe()
	if err != nil {
		t.Errorf("Ocorreu um erro.")
	}
}