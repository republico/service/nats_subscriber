package main

import (
	"flag"
	"fmt"

	"gitlab.com/republico/library/go/util/pkg/exception"
	"gitlab.com/republico/service/nats_subscriber/internal/app/sub"
)

func main() {
	environmentVariableAccount := flag.String("key", "ACCOUNT", "Nome da variável de ambiente com o nome da conta do Azure Storage.")
	environmentVariableKey := flag.String("account", "KEY", "Nome da variável de ambiente com a senha do Azure Storage.")
	serverNats := flag.String("server", "localhost:4222", "Endereço(s) do(s) servidor(es) NATS.")
	queue := flag.String("queue", "republico", "Nome do queue do subscriber. Usado para criar grupos de subscribers.")
	subject := flag.String("subject", "*", "Nome do subject a ser inscrito.")
	separator := flag.String("separatorSubject", "|", "Separador usado para separar os componentes do subject.")
	timeout := flag.Int("timeout", 0, "Tempo em segundos entre a leitura das mensagens para o término do programa.")
	waitTimeout := flag.Bool("wait", false, "O contador de tempo para o encerramento do programa, deve esperar o recebimento da primeira mensagem para iniciar?")
	sizeLimit := flag.Int("size", 128, "Tamanho limite de um arquivo em megabytes.")
	interval := flag.Int("interval", 1, "Intervalo de quantidade de mensagens para a operação de gravação.")
	maxTime := flag.Int("maxTime", 0, "Tempo máximo em segundos de execução do programa.")

	flag.Parse()

	sub.Config = sub.ConfigStruct{
		EnvironmentVariableAccount: *environmentVariableAccount,
		EnvironmentVariableKey: *environmentVariableKey,
		ServerNats: *serverNats,
		Queue: *queue,
		Subject: *subject,
		SeparatorSubject: *separator,
		Timeout: *timeout,
		WaitTimeout: *waitTimeout,
		SizeLimit: *sizeLimit,
		Interval: *interval,
		MaxTime: *maxTime,
	}

	fmt.Println("Starting receiving messages")

	err := sub.Subscribe()
	exception.HandlerError(err)

	fmt.Println("Stopping receiving messages")
}