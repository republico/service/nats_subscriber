FROM alpine:3.8

LABEL version="1.0"
LABEL maintainer="Vagner Praia <vagnerpraia@gmail.com>"
LABEL description="Container com o programa subscriber do NATS."

RUN mkdir /app

COPY ./bin/subscriber /app/subscriber

RUN /app/subscriber \ 
    -q "Subscriber NATS" \
    -s "localhost:4222" \
    -subject "*" \
    -messages "0" \
    -second "0"